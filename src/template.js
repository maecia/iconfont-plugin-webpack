'use strict';

const TEMPLATE = `
@use 'sass:map';
@use '../variables/fonts-variables' as *;

/* stylelint-disable */
/* creates the font face tag if the variable is set to true (default) */
@if $create-font-face == true {
  @font-face {
   font-family: "__FAMILY__";
   src: url('__RELATIVE_FONT_PATH__/__FAMILY__.eot'); /* IE9 Compat Modes */
   src: url('__RELATIVE_FONT_PATH__/__FAMILY__.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
      url('__RELATIVE_FONT_PATH__/__FAMILY__.woff') format('woff'), /* Pretty Modern Browsers */
      url('__RELATIVE_FONT_PATH__/__FAMILY__.ttf')  format('truetype'), /* Safari, Android, iOS */
      url('__RELATIVE_FONT_PATH__/__FAMILY__.svg') format('svg'); /* Legacy iOS */
  }
}

/* creates icon classes for each individual loaded svg (default) */
@if $create-icon-classes == true {
[class^="sid2-"],
[class*=" sid2-"] {
    font-family: '__FAMILY__', serif;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    @each $icon, $content in map.get($iconfont-data, "__FAMILY__") {
      &.#{$icon-prefix}#{$icon}::before {
        content: iconfont-item("__FAMILY__/#{$icon}");
      }
    }
  }
}
/* stylelint-enable */
`;

function toSCSS(glyphs) {
  return JSON.stringify(glyphs, null, '\t')
    .replace(/\{/g, '(')
    .replace(/\}/g, ')')
    .replace(/\\\\/g, '\\');
}

module.exports = function (args) {
  const family = args.family;
  const pathToFonts = args.fontPath;
  const glyphs = args.unicodes.reduce(function (glyphs, glyph) {
    glyphs[glyph.name] = '\\' + glyph.unicode.charCodeAt(0).toString(16).toLowerCase();
    return glyphs;
  }, {});
  const data = {};
  data[family] = glyphs;

  const replacements = {
    __FAMILY__: family,
    __RELATIVE_FONT_PATH__: '~fonts/sid2'
  };

  const str = TEMPLATE.replace(RegExp(Object.keys(replacements).join('|'), 'gi'), function (matched) {
    return replacements[matched];
  });

  return [str].join('\n\n');
};
