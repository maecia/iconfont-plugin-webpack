const TEMPLATE = `
/* should the @font-face tag get created? */
/* stylelint-disable */
$create-font-face: true !default;

/* should there be a custom class for each icon? will be .filename */
$create-icon-classes: true !default;

/* what is the common class name that icons share? in this case icons need to have .icon.filename in their classes */
/* this requires you to have 2 classes on each icon html element, but reduced redeclaration of the font family */
/* for each icon */
$icon-common-class: 'sid2' !default;

/* if you whish to prefix your filenames, here you can do so. */
/* if this string stays empty, your classes will use the filename, for example */
/* an icon called star.svg will result in a class called .star */
/* if you use the prefix to be 'icon-' it would result in .icon-star */
$icon-prefix: 'sid2-' !default;

/* helper function to get the correct font group */
@function iconfont-group($group: null) {
  @if (null == $group) {
    $group: nth(map-keys($iconfont-data), 1);
  }
  @if (false == map.has-key($iconfont-data, $group)) {
    @warn 'Undefined Iconfont Family!';
    @return ();
  }
  @return map.get($iconfont-data, $group);
}

/* helper function to get the correct icon of a group */
@function iconfont-item($name) {
  $slash: string.index($name, '/');
  $group: null;
  @if ($slash) {
    $group: string.slice($name, 0, $slash - 1);
    $name: string.slice($name, $slash + 1);
  } @else {
    $group: nth(map-keys($iconfont-data), 1);
  }
  $group: iconfont-group($group);
  @if (false == map.has-key($group, $name)) {
    @warn 'Undefined Iconfont Glyph!';
    @return '';
  }
  @return map.get($group, $name);
}

/* complete mixing to include the icon */
/* usage:  */
/* .my_icon{ @include iconfont('star') }  */
@mixin iconfont($icon) {
  $slash: string.index($icon, '/');
  $group: null;
  @if ($slash) {
    $group: string.slice($icon, 0, $slash - 1);
  } @else {
    $group: nth(map-keys($iconfont-data), 1);
  }
  &:before {
    font-family: $group;
    font-style: normal;
    font-weight: 400;
    content: iconfont-item($icon);
  }
}
/* stylelint-enable */
`;

function toSCSS(glyphs) {
  return JSON.stringify(glyphs, null, '\t')
    .replace(/\{/g, '(')
    .replace(/\}/g, ')')
    .replace(/\\\\/g, '\\');
}

module.exports = function (args) {
  const family = args.family;
  const pathToFonts = args.fontPath;
  const glyphs = args.unicodes.reduce(function (glyphs, glyph) {
    glyphs[glyph.name] = '\\' + glyph.unicode.charCodeAt(0).toString(16).toLowerCase();
    return glyphs;
  }, {});
  const data = {};
  data[family] = glyphs;

  const replacements = {
    __FAMILY__: family,
    __RELATIVE_FONT_PATH__: '~fonts/sid2'
  };

  const str = TEMPLATE.replace(RegExp(Object.keys(replacements).join('|'), 'gi'), function (matched) {
    return replacements[matched];
  });

  return [
    `@use 'sass:meta';
     @use 'sass:map';
     @use "sass:string";

    $iconfont-data: map.merge(if(meta.global-variable-exists('__iconfont__data'), $iconfont-data, ()), ${toSCSS(data)});`,
    str
  ].join('\n\n');
};
